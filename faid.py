import re
import urllib.request, urllib.error, urllib.parse
import requests
from bs4 import BeautifulSoup as bs
import sys
import math
import traceback
import os
import time
import pickle
import signal
from requests import Response
from enum import Enum
from typing import NewType, Callable
from pathlib import PurePath, Path
import json
from datetime import datetime
import tzlocal

filenameRegex = r"(?P<timestamp>[0-9]*)\.(?P<username>[^_]*)_(?P<title>.*)"
UsersRegex = r"([0-9]+)\s+<b>registered"

FADttmFmt = "%b %d, %Y %I:%M %p"

from faendpoints import frontPageEndpoint, submissionEndpoint

SubmissionId = NewType('SubmissionId', int)
Timestamp = NewType('Timestamp', int)

localtz = tzlocal.get_localzone()

fetches = 0

class ServerError(Exception):
	pass


class SubmissionStatus(Enum):
	OK = 1
	DELETED = 2
	ERROR = 3
	UNKNOWN = 4

class SubmissionInfo:
	def __init__(self, id: SubmissionId, status: SubmissionStatus, submitter: str = None, timestamp: Timestamp = None, err: str = None, note: str = None, filename: str = None):
		self.id: SubmissionId = id
		self.status: SubmissionStatus = status
		self.submitter: str = submitter
		self.timestamp: Timestamp = timestamp
		self.err = err
		self.filename = filename
		self.notes = []
		self.dateFetched = datetime.now()

		if note is not None:
			self.add_note(note)

	def add_note(self, note:str):
		self.notes.append(note)
	
	def print_notes(self, indent="\t"):
		for n in self.notes:
			print(f"{indent}{n}")
		if self.err:
			print(f"{indent}ERR: {self.err}")


	def __str__(self):
		s = f"[ {self.id} {self.status.name} @ {self.timestamp} by {self.submitter}"
		if len(self.notes) > 0:
			s += f" {len(self.notes)}N"
		if self.err is not None:
			s += f" <ERR>"
		if self.filename is not None:
			s += f" {self.filename}"
		s += " ]"
		return s
	
	def __repr__(self):
		return str(self)

	

class TimestampEntry:
	def __init__(self, timestamp: Timestamp, submissions=None):
		self.timestamp: Timestamp = timestamp
		self.submissions: list[SubmissionInfo] = []
		self.add(submissions)

	def __len__(self):
		return len(self.submissions)

	def add(self, submissions):
		if isinstance(submissions, list):
			self.submissions.extend(submissions)
		elif isinstance(submissions, SubmissionInfo):
			self.submissions = [submissions]
	
	def get(self, submitter):
		for s in self.submissions:
			if s.submitter == submitter:
				return s
		
		return None
	
	def get_all(self):
		return self.submissions




class TimestampCollection:
	def __init__(self):
		self.items: dict[Timestamp, TimestampEntry] = {}
	
	def add(self, timestamp: Timestamp, submission: SubmissionInfo):
		tsEntry = None
		if timestamp not in self.items:
			## Make a new one
			tsEntry = TimestampEntry(timestamp, submission)
			self.items[timestamp] = tsEntry
		else:
			tsEntry = self.items[timestamp]
			tsEntry.add(submission)
	
	def get(self, timestamp: Timestamp, submitter: str) -> SubmissionInfo:
		tsEntry = self.items[timestamp]
		return tsEntry.get(submitter)
	
	def __len__(self):
		return len(self.items)




cookieJar = None



rootdir = os.path.dirname(os.path.realpath(sys.argv[0]))
session = None

## TODO: refactor these to use Endpoint classes.  To do this, we must allow endpoint classes to use cookies.
##       Ideally we could also use a global rate limit for all endpoints using the same host
faUrl   = "https://www.furaffinity.net"
viewUrl = "https://www.furaffinity.net/view/"

## Setup for url to filename cache
sid2sub: dict[SubmissionId, SubmissionInfo] = {}
mapfileName = "url2filemap.pkl"

## url id of last submission at time of writing, fileid should be in map
latestSubmission = 45799932
firstSubmission = 1

downloadSaveLimit = 3  ## How many files to get before automatically saving
downloadCounter = 0

getCooldown = 1.1    ## number of seconds to wait between page gets (FA has crawl-delay: 1)
activeGetCooldown = 1.5
lastGetTime = 0

def loadMap():
	global sid2sub
	mapfile = open(mapfileName, 'rb')
	sid2sub = pickle.load(mapfile)
	mapfile.close()
	print(f"Loaded sid2sub map with {len(sid2sub)} entries")

def getLatestSubmissionId() -> int:
	res = frontPageEndpoint.send()
	if res.ok:
		html = res.text
		soup = bs(html, "html.parser")

		id_str = soup.find("figure", class_="t-image")["id"]
		id = int(id_str.replace("sid-", ""))

		return id
	else:
		raise Exception("Could not access FurAffinity")


def main(filename: str =None, sid: SubmissionId=None):
	global sid2sub
	global downloadCounter
	global latestSubmission

	try:
		# load url2file map from pkl file, if exists
		loadMap()
	except Exception as e:
		## Otherwise, create an empty map
		sid2sub = {}
		print("No mapfile found, initializing empty map")

	loggedIn, session = authenticate()

	## Exit if we cannot log in.  If we are denied access to a page because we are not
	## logged in (because of content filter or artist settings), we may accidentally
	## mark that as an invalid submission, and then we will not try to look into it
	## in successive runs if we actually are logged in.
	
	## == TODO: Make robust checks of page errors to determine if submission does not
	##          exist or we are blocked by content filters / artist settings.
	if not loggedIn:
		print("Could not log in, exiting.")
		return

	latestSubmission = getLatestSubmissionId()
	print(f"Found latest submission id {latestSubmission}")

	if filename is None and sid is None:
		filename = input("Please enter a target filename: ").strip()
		# e.g. 1636740096.kilodyne_lisastompsnua1.png
		# If we have been given a path, then remove the path part
		filename = PurePath(filename).name
	
	if filename is not None:
		filename = PurePath(filename).name
		sub = getSubFromFilename(filename)
	elif sid is not None:
		sub = searchSubTreeForSid(sid)

	print(f"Found target item: {sub}")

	## write out url2file map
	print("  saving...")
	saveMap()

def parseFilename(filename:str):
	## Parse filename to get timestamp and submitter
	## The typical, current format for filenames is:
	##   [timestamp].[username]_[filename(+ext)]
	## Usernames containing underscores appear to have those underscores omitted in filenames 
	## and other backend uses, making parsing these unambiguous.
	## This format begins with submission  69626  (Mar 8, 2006 11:32 AM).
	## (Submissions 69624 and 69625 are deleted)

	## BUT -- at some point before Jun 18 2006 (timestamp 1150667715), filenames were stored
	## in the format 
	##   [timestamp].[username].[filename(+ext)]
	## with a '.' separating the username from the filename.  Usernames *can* include periods, 
	## which *are* included in the "backend" username (unlike underscores), which makes parsing 
	## these older files ambiguous. underscores are still ommitted. If we assume that an underscore 
	## always separates the username and filename (using the modern parsing method), then we might 
	## incorrectly include part of the filename in the parsed username if the filename contains underscores.
	## This file format exists
	##   starting with sid 943
	##   up to at and including 69623  (Mar 6, 2006 05:35 AM)



	## Submission 942 / 943 form the boundaries between these two filename eras.  It seems there is
	## also a several month break between them - July 2005 to Dec 2005 - implying heavy server
	## maintenance window where this format was changed
	##   Actually, lots of dates and filename formats are jumbled up before on/before sid ~15000.  Historically,
	##   FA was offline and a large DB/code restructure took place between July and Dec 2005.  I'm guessing
	##   that during this restructure, old submissions were uploaded into the new system with arbitrary sids,
	##   but with historic upload dates kept.

	## NOTE - for a time on/after submission 617 (?) It seems
	## FA began using a filename format like:
	##   [username]_[timestamp]_[filename(+ext)]
	## However, for these, if there are underscores in the username, they are preserved but as spaces. 
	## (This may be an artifcat of this user re-uploading stuff though??)
	##    These exist as far back as 617 - before the arbitrary danielkay zone (~784).
	##    boundary?:  617 ([]_[]_[]) / 615 (arbitrary)
	##   up to (incl) 942

	## Randomly, some older files (before 573) DO have the [].[].[] format? (e.g. 14)


	m = re.match(filenameRegex, filename)
	if m is None:
		raise Exception(f"Could not parse timestamp and submitter from filename {filename}")

	return int(m.group("timestamp")), m.group("username"), m.group("title")

def searchSubTree(target: SubmissionInfo, compareFunc: Callable[[SubmissionInfo, SubmissionInfo], int]) -> SubmissionInfo:
	global sid2sub
	global latestSubmission
	global fetches

	# target_ts = target.timestamp
	# submitter = target.submitter

	## Determine the initial boundaries of the search.  The rightmost bound *can* be larger than
	## the latest submission -- if anything beyond that is tested during the search, we will short
	## circuit and force the next search to be smaller (as though we found an item that had a later)
	## timestamp).  
	## This boundary determinitation ensures that our search tree - and the items we cache - we
	## always aligned to powers of 2.  As the latestSubmission grows, we can still search aligned
	## to that cache and still reap the rewards of the cache.
	L = 1                     
	R = 2 ** math.ceil(math.log2(latestSubmission))

	resolutions = 0
	fetches = 0

	new_url_id = int(math.floor((L + R)/2.0))


	while True:
		print("new_url_id: [0b{}] {}".format(format(new_url_id, "b"), new_url_id))
		print("L: {}  | new_url_id: {}  | R: {}".format(L, new_url_id, R))

		if L > R:
			print("Could not find a result (submission may have been deleted)")
			break

		url_id = new_url_id	

		if url_id > latestSubmission:
			print("Outside of range, assuming associated time is larger than target")
			R = url_id
			new_url_id = int(math.floor((L + R)/2.0))
			continue

		# get file id from url id
		actual_url_id, min_id, max_id, found_sub = getClosestFileFromUrl(url_id)

		resolutions += 1
		print("Successfull resolutions: {}".format(resolutions))
		
		print(f"Found submission with timestamp {found_sub.timestamp}")
		for n in found_sub.notes:
			print (f"\t{n}")

		# found_ts = sub.timestamp

		## test against target file id

		compResult = compareFunc(target, found_sub)

		if compResult >= 1:
			R = min_id - 1
		elif compResult <= -1:
			L = max_id + 1
		else:
			print (f"Descent took {fetches} remote fetches")
			return found_sub

		new_url_id = int(math.floor((L + R)/2.0))

		

def getSubFromFilename(filename: str):
	target_ts, submitter, _ = parseFilename(filename)
	target_sub = SubmissionInfo(None, status=SubmissionStatus.UNKNOWN, submitter = submitter, timestamp=target_ts)

	def sfunc(target_sub, found_sub):
		## Return the value of the found relative to the target
		if found_sub.timestamp > target_sub.timestamp:
			print("target ({}) < file_id ({})".format(target_sub.timestamp , found_sub.timestamp))
			return 1
		elif found_sub.timestamp < target_sub.timestamp:
			print("target ({}) > file_id ({})".format(target_sub.timestamp , found_sub.timestamp))
			return -1
		else:
			print("timestamp match! url_id found: {}".format(found_sub.id))
			return 0
	
	found_sub = searchSubTree(target_sub, sfunc)

	if (found_sub.submitter != target_sub.submitter):
		print (f"=== Found submitter {found_sub.submitter} does not match expected submitter {target_sub.submitter}, exploring neighbors...")
		return exploreSubmissionNeighbors(found_sub, target_sub.submitter)
	else:
		return found_sub

def searchSubTreeForSid(sid: SubmissionId):
	"""
	In general, to gather a submission from a submission id, you should use `getSubFromSID`, which
	will look at the local cache or retrieve the item directly from the submission URL.

	This function is meant to search the tree in the same way as we search for a timestamp (or do any)
	other tree search).  This allows us to visit all submission nodes in the tree on the way to the
	target node, fetching their data from FA if necessary.  This is mainly useful for testing or filling
	the cache in a more directed way.
	"""

	target_sub = SubmissionInfo(sid, status=SubmissionStatus.UNKNOWN)

	def sfunc(target_sub, found_sub):
		## Return the value of the found relative to the target
		if found_sub.id > target_sub.id:
			print("target sid ({}) < found sid ({})".format(target_sub.id , found_sub.id))
			return 1
		elif found_sub.id < target_sub.id:
			print("target sid ({}) > found sid ({})".format(target_sub.id , found_sub.id))
			return -1
		else:
			print("sid match! sid found: {}".format(found_sub.id))
			return 0

	return searchSubTree(target_sub, sfunc)



def exploreSubmissionNeighbors(found_submission: SubmissionInfo, submitter: str) -> SubmissionInfo:
	### It may be that multiple users submitted items during the same second, in which case they will
	### have the same timestamp.  When we do a search based on timestamp, it's possible then that
	### We will find a result for that timestamp, but from a different user.  When that happens, we must
	### search around that submission until we find the one from the user we want.

	sid = found_submission.id

	## First search for neighbors earlier than the found one (arbitrary)
	while True:
		sid -= 1
		nsub = getSubFromSID(sid)
		if nsub.timestamp != found_submission.timestamp:
			## we've reached the end of the neighbors sharing our timestamp in this direction.
			print (f"Reached submission neighbor boundary (-) at sub {nsub.id} with timestamp {nsub.timestamp}")
			break
		else:
			if nsub.submitter == submitter:
				## We found the entry matching the submitter!
				return nsub
	
	## If we didn't find the submission we're looking for, then search for neighbors after than the found one (arbitrary)
	sid = found_submission.id
	while True:
		sid += 1
		nsub = getSubFromSID(sid)
		if nsub.timestamp != found_submission.timestamp:
			## we've reached the end of the neighbors sharing our timestamp in this direction.
			print (f"Reached submission neighbor boundary (+) at sub {nsub.id} with timestamp {nsub.timestamp}")
			break
		else:
			if nsub.submitter == submitter:
				## We found the entry matching the submitter!
				return nsub
	
	## If we never found the expected submission, then return None
	print ("==== Could not find the target submission at all!")
	return None




def getSubFromSID(sid: SubmissionId) -> SubmissionInfo:
	global downloadCounter

	print("== Looking for submission from sid {}".format(sid))

	sub = None

	if sid in sid2sub:
		sub = sid2sub[sid]
		print("Found in map! ({})".format(sub))
	else:
		print("Getting data from server...")
		url = viewUrl + str(sid)
		html = getpage(url)
		soup = bs(html, "html.parser")


		try :
			if soup.title.string == "System Error":
				if "The submission you are trying to find is not in our database" in soup.find_all("font")[-1].prettify():
					## Create submission with error state
					print("Found system error (submission probably deleted)!")  
					sub = SubmissionInfo(sid, SubmissionStatus.DELETED)
				else:
					saveHtml(sid, html)
					raise Exception ("Unknown System error found")
			elif len([td for td in soup.find_all("td") if td.string is not None and td.string.strip() == "System Message"]) > 0:
				## Example:
				## <table cellpadding="3" cellspacing="1" border="0" width="95%" class="maintable">
				## 	<tbody>
				##   <tr>
				## 		<td align="center" class="cat">
				## 			System Message
				## 		</td>
				## 	 </tr>
				## 	 <tr valign="middle" align="center">
				## 		<td class="alt1">
				## 			<div style="padding: 10px;">
				## 				<p class="link-override">The page you are trying to reach is currently pending deletion by a request from its owner.</p>
				##          </div>
				## 			<br>
				## 			[ 
				##            <a href="javascript:history.go(-1)">Click here to go back...</a>
				##          ]
				## 			<br>
				## 			<br>
				## 		</td>
				## 	 </tr>
				##  </tbody>
				## </table>	
				saveHtml(sid, html)
				tds = [td for td in soup.find_all("td") if td.string is not None and td.string.strip() == "System Message"]
				if len(tds) > 0:
					td = tds[0]
					errorMessage = td.find_parent("tr").find_next_sibling("tr").find("td").find("div").find("p").string
					print(f"Found system message: {errorMessage}")
					sub = SubmissionInfo(sid, SubmissionStatus.ERROR, err=errorMessage)
				else :
					print(f"Found some other error?")
					sub = SubmissionInfo(sid, SubmissionStatus.ERROR, err="unknown error encountered (see saved html)")

			else:
				## Try to parse the timestamp and submitter from the html, specifically the Download link:
				## e.g. https://d.furaffinity.net/art/alkora/1436887262/1133739096.alkora_background.jpg
				##                                    ^^^^^^ ^^^^^^^^^^ ^^^^^^^^^^
				##                                    |      |          |
				##                       submitter ---+      |          |
				##                        timestamp (newer) -+          |
				##                            timestamp (original?) ----+
				##
				## Sometimes the two timestamps don't match, and the url portion is larger than the file portion.
				## This might have something to do with re-uploaded files?
				saveHtml(sid, html)

				fpath = soup.find("div", class_="actions").find(lambda t: t.string=='Download').find("a")["href"]
				fpathparts = fpath.split("/")
				fname = fpathparts[-1]
				url_ts = int(fpathparts[-2])
				timestamp = None

				notes = []

				filename_ts = None
				try: 
					## Parsing the information out of the filename 
					filename_ts = int(fname.split(".")[0])
				except Exception as e:
					notes.append("Could not parse timestamp from filename")

				## Older filenames may not follow any convention at all, so even if we get something
				## that looks like a timestamp from the filename, it may not be valid.
				## Here we attempt to validate what we find.

				## timestamps from before FA existed should not be trusted
				fa_birth_ts = datetime(2004, 1, 1).timestamp()

				if filename_ts is not None and filename_ts < fa_birth_ts:
					notes.append(f"""Parsed apparent timestamp {filename_ts} from file, but it appears to be older than furaffinity itself.  Not considering.""")
					filename_ts = None

				## If the url time and file timestamp match, we can probably trust them
				if filename_ts is not None and filename_ts == url_ts:
					timestamp = filename_ts

				else:
					## Otherwise we should check the "Posted" date...
					## Try to verify by seeing if it matches the expected range based on the "Posted" date.
					##   Note that FA does the work of timezone consideration for us - the "Posted" date
					##   on the page is timezone aware, thus if we construct a timezone aware datetime,
					##   it will evaluate to the same timestamp as the uploaded file.
					##   (This does assume the FA profile / crawler browser / python installation are all
					##   using the same timezone.)

					## Depending on FA profile config, the date string may either be in the hover popup
					## or displayed in the element content.  We'll try to recognize this.
					posted_dates = [
						soup.find(class_="popup_date").string,
						soup.find(class_="popup_date")["title"]
					]

					posted_ts = None
					posted_str = None
					for d in posted_dates:
						try:
							posted = datetime.strptime(d, FADttmFmt).astimezone()
							posted_str = d
							posted_ts = posted.timestamp()
						except ValueError as ve:
							pass

					## First check against the filename_ts
					if posted_ts is not None and filename_ts is not None:
						ts_match = matchPostedDate(posted_ts, filename_ts, notes)
						if ts_match:
							## We can trust the filename timestamp as proceed as normal
							timestamp = filename_ts
						else:
							## there is a timestamp mismatch.  We shouldn't trust the filename timestamp?
							notes.append(f"Posted timestamp, {posted_ts} ({posted_str}), does not contain filename timestamp ({filename_ts})")
							filename_ts = None
					
					## See if the url timestamp matches the posted timestamp
					if timestamp is None and posted_ts is not None:
						ts_match = matchPostedDate(posted_ts, url_ts, notes)
						if ts_match:
							## If the url timestamp is within the same minute as the posted timestamp, then we can trust it.
							notes.append(f"Timestamp in url matches \"Posted\" time, using the url timestamp")
							timestamp = url_ts
						else:
							## there is a timestamp mismatch.  We shouldn't trust the url timestamp?
							notes.append(f"Posted timestamp, {posted_ts} ({posted_str}), does not contain url timestamp ({url_ts})")
							url_ts = None

							## TODO
							## (The url mismatch may be due to resubmitting the file, in which case it may represent the
							## most recent modification time.  In that case, does the image name change too?? Need to test)
				
							## TODO
							## If everything else has failed, then:
							## we could use the "Posted" time on the submission page, though this
							## only has resolution of one minute, which may lead to issues with the search algo.
							## It is probably possible to be aware of time "windows", similar to how we handle
							## and epand the search for deleted submissions, though it may be trickier when
							## we actually do have some submission info that we don't want to skip over?
							## Maybe when we perform such a search, we consider all items in the window and
							## attempt to match by submitter name - though we are likely to encounter duplicates
							## for prolific submitters
							##
							## Instead for now, we'll just give up and keep timestamp = None

				## Because of the issues discussed in the parseFilename() comments, it's possible
				## that we might accidentally parse part of the filename into the submitter name
				## for older files containing underscores.  Thus, we will always try to use the 
				## submitter name from the URL instead of trusting the filename.
				submitter = fpathparts[-3]

				if timestamp is None:
					sub = SubmissionInfo(sid, status=SubmissionStatus.ERROR, timestamp=None, submitter=submitter, filename=fname, err="Submission found, but could not verify timestamp.")
				else:
					sub = SubmissionInfo(sid, status=SubmissionStatus.OK, timestamp=timestamp, submitter=submitter, filename=fname)

				for n in notes:	
					sub.add_note(n)

		except Exception as e:
			print("Exception when parsing html!")
			saveHtml(sid, html)
			traceback.print_exc()
			f = open("err.txt", "w")
			f.write(str(e) + "\n\n")
			f.write("html: {}\n\n".format(html))
			f.close()
			saveMap()
			raise e

		## add to dict, for both successes and failures
		sid2sub[sid] = sub
		print("Found ({})".format(sub))

		## save map every so often as we get data from the server
		downloadCounter += 1
		if downloadCounter == downloadSaveLimit:
			print("  saving map...")
			downloadCounter = 0
			saveMap()

	return sub

def matchPostedDate(posted_ts, ts, notes):
	### Checks if the given timestamp matches the "Posted" date.
	## Time zones are weird.  Making a datetime from a timestamp
	## does not take into account that DST definitions have changed over time,
	## and therefore creates datetimes from historic timestamps under
	## the current timezone rules, not the ones in place at the time.
	## FA does, so the posted date matches the actual historic timezone,
	## meaning that comparing against timezones may be off by an hour.
	
	## Instead of solving this properly, we will just allow it to be off by
	## an hour (aligned to the same minute)

	## TODO: this might help fix the historic timezone/dst issue: https://stackoverflow.com/a/40769643

	diff = abs(posted_ts - ts)
	if diff < 60:
		## comparison went as expected, no notes
		return True
	elif diff >= 3600 and diff <= 3660:
		## If it's an hour off, but still during the same minute, we'll allow it
		notes.append(f"posted_ts {posted_ts} is off expectation {ts} by one hour, but still matches (dst/timezone issues)")
		return True
	else:
		return False

def saveMap(filename=mapfileName):
	## write out url2file map
	mapfile = open(filename, 'wb')
	pickle.dump(sid2sub, mapfile)
	mapfile.close()
	print("   Saved!")


def getClosestFileFromUrl(base_url_id):
	direction = -1
	size = 0
	sub = None
	url_id = base_url_id
	min_id = base_url_id
	max_id = base_url_id

	print("Getting closest file to url_id {}".format(base_url_id))

	while sub is None or sub.status != SubmissionStatus.OK:
		sub = getSubFromSID(url_id)

		if url_id < min_id:
			min_id = url_id
		elif url_id > max_id:
			max_id = url_id

		if sub.status != SubmissionStatus.OK:
			direction *= -1
			size      +=  1
			url_id = url_id + (direction * size)

			print(f"Submission invalid ({sub})")
			for n in sub.notes:
				print (f"\t{n}")
			print("expanding search to {}".format(url_id))

	print("Found!")

	return (url_id, min_id, max_id, sub)



## Given a url and a session, returns the HTML from the HTTP Reply
def getpage(url, session=session):
	global lastGetTime
	global fetches 

	loggedIn = False if session is None else True

	if session is None:
		loggedIn, session = authenticate()

	if not loggedIn:
		print("Not logged in to any account!  Not getting page.")
		return ''

	print("Getting page {}".format(url))

	## Check getCooldown
	now = time.time()
	sinceLast = now - lastGetTime

	# print("now: {}    sinceLast: {}".format(now, sinceLast))

	if sinceLast < getCooldown:
		dur = getCooldown-sinceLast
		print("sleeping for {} seconds...".format(dur))
		time.sleep(dur)

	response = session.get(url)
	
	lastGetTime = time.time()
	# print("Done; setting lastGetTime to : {}".format(lastGetTime))

	fetches += 1

	return response.content

def authenticate():
	global session
	global cookieJar
	global getCooldown
	loggedIn = False

	if session is None:
		s = requests.session()
		session = s

		if cookieJar is None:
			gatherCookie()
			if cookieJar is None:
				cookieString = input("Please copy/paste cookie value from HTTP Request header: ")
				cookieJar = cookiejarFromString(cookieString)

		s.cookies = cookieJar

		## Test if we're logged in by examining the top-right status bar
		try:
			html = getpage(faUrl, s)
			soup = bs(html, "html.parser")

			ul = soup.select("td.header_bkg > ul.dropdown.dropdown-horizontal.dropdown-left")[0]
			href = ul.li.a['href']

			if href == "/register/":
				## Browser has not logged in, is in Guest mode
				print("Could not log in, continuing in Guest mode")
			elif href.startswith("/user/"):
				## Logged in!
				print("Logged in as {}".format(href.split("/")[-2]))
				loggedIn = True
			else:
				## Not sure what happened
				raise Exception("Authentication unsure, perhaps layout HTML has changed (href={})".format(href))

			## Gather information about the number of registered users.  FA prefers us to limit bot activity to when there are
			## less than 10,000 registered users online.  We won't completely stop, but we will increase the cooldown duration a bit.
			footerStr = str(soup.find(class_="footer").find("center"))
			m = re.search(UsersRegex, footerStr)
			if m is not None:
				activeUsers = int(m.group(1))
				if activeUsers > 10_000:
					print("Site is under active load, using longer cooldown")
					getCooldown = activeGetCooldown
			else:
				print("Could not determine active users, assuming active site and using longer cooldown")
				getCooldown = activeGetCooldown

		except Exception as e:
			print("ERROR: Exception ocurred when testing authentication!")
			print(traceback.format_exc(limit=2))
			return (False, None)

	else:
		loggedIn = True

	return (loggedIn, session)

def gatherCookie():
	global cookieJar
	try:
		with open("cookie") as cookieFile:
			cookieString = cookieFile.read()
		if cookieString == "":
			raise Exception("No cookie defined in cookie file")
		cookieJar = cookiejarFromString(cookieString)
	except Exception as e:
		print ("Could not read cookie string")
		raise e

def cookiejarFromString(cookieString):
	cookieDict = {}

	if len(cookieString) > 0:
		cookieString = cookieString.strip()
		cookiePairs = re.split(";\s*", cookieString)

		## create dict from list of key=value pairs
		for p in cookiePairs:
			k, v = p.split("=")
			cookieDict[k] = v

	return requests.utils.cookiejar_from_dict(cookieDict)

def exploreSpace(depth):
	global sid2sub

	try:
		# load url2file map from pkl file, if exists
		mapfile = open(mapfileName, 'rb')
		sid2sub = pickle.load(mapfile)
		mapfile.close()
	except Exception as e:
		## Otherwise, create an empty map
		sid2sub = {}
		print("No mapfile found, initializing empty map")

	loggedIn, session = authenticate()

	## Exit if we cannot log in.  If we are denied access to a page because we are not
	## logged in (because of content filter or artist settings), we may accidentally
	## mark that as an invalid submission, and then we will not try to look into it
	## in successive runs if we actually are logged in.
	
	## == TODO: Make robust checks of page errors to determine if submission does not
	##          exist or we are blocked by content filters / artist settings.
	if not loggedIn:
		print("Could not log in, exiting.")
		return

	#target = int(input("Please enter a target file id: ")) ##1507549741  ## target file id

	L = 1                      ## First submission time is 1133739096  (Nov 26 2005)
	R = latestSubmission  

	print(f"estimated hits: {2 ** depth}")


	explore(L, R, depth)

	saveMap()

def explore(L, R, depth, path="", hits={"hits": 0}):
	if depth == 0:
		return
	else:
		## Do a single check for this layer, then descend into children on each side.
		## After this check, we will descend to the left and to the right

		## 
		test_url_id = int(math.floor((L + R)/2.0))

		sub = getSubFromSID(test_url_id)	
		timestamp = sub.timestamp
		hits["hits"] += 1 
		numhits = hits["hits"]
		print(f"({path}) Found fileid {timestamp} for url {test_url_id} at depth {depth} (hit {numhits})")

		## Descend left
		explore(L, test_url_id, depth - 1, path+"L", hits)
		## Descend right
		explore(test_url_id, R, depth - 1, path+"R", hits)


def saveHtml(sid: SubmissionId, html:bytes):
	htmldir = Path("html")
	htmldir.mkdir(exist_ok=True)

	fname = f"{sid}.html"
	fpath = htmldir.joinpath(fname)

	print (f"Saving html for {sid} in {fpath}")

	f = open(fpath, "wb")
	f.write(html)
	f.close()



def handle_SIGINT(signal, frame):
	## cleanup gracefully when given SIGINT (Ctrl + C)
	print("Got SIGINT, cleaning...")

	saveMap()

	sys.exit(0)

## Register signal handler
signal.signal(signal.SIGINT, handle_SIGINT)

def surgery():
	global sid2sub
	loadMap()

	return

if __name__ == "__main__":
	if len(sys.argv) > 1 and sys.argv[1] == "-l":
		## List all items in the map
		loadMap()
		keys = sorted(list(sid2sub.keys()))
		for k in keys:
			print(sid2sub[k])
	elif len(sys.argv) > 1 and sys.argv[1] == "-d":
		## List all items in the map
		loadMap()
		sid = int(sys.argv[2])
		sub = sid2sub[sid]
		print(sub)
		sub.print_notes()
	elif len(sys.argv) > 1 and sys.argv[1] == "-v":
		## Verify timstamp integrity
		loadMap()
		keys = sorted(list(sid2sub.keys()))
		prevTimestamp = None
		prev = None
		for k in keys:
			sub = sid2sub[k]
			ts = sub.timestamp
			if ts is not None:
				if prevTimestamp is not None:
					## Ensure this timstamp is > the previous one
					if prevTimestamp > ts:
						print(f"TIMESTAMP INTEGRITY ERROR: Timestamp {ts} for sub {sub.id} is less than previous timestamp:")
						print(f"\tthis: ({datetime.fromtimestamp(ts, localtz).strftime(FADttmFmt)}) {sub}")
						sub.print_notes("\t\t")
						print(f"\tprev: ({datetime.fromtimestamp(prevTimestamp, localtz).strftime(FADttmFmt)}) {prev}")
						prev.print_notes("\t\t")
				prevTimestamp = ts
				prev = sub

	elif len(sys.argv) > 1 and sys.argv[1] == "-f":
		filename = sys.argv[2]
		main(filename=filename)
	elif len(sys.argv) > 1 and sys.argv[1] == "-s":
		sid = int(sys.argv[2])
		main(sid=sid)
	else:
		surgery()
		# main(sid=1024)

		# main()
		# exploreSpace(11)