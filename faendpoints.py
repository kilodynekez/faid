from endpoints import Endpoint

default_rate_limit = 1000 ## ms

submissionEndpoint = Endpoint(
    "https://www.furaffinity.net/view/{sid}",
    rate_limit_ms=default_rate_limit
)

galleryEndpoint = Endpoint(
    "https://www.furaffinity.net/user/{username}",
    rate_limit_ms=default_rate_limit
)

journalEndpoint = Endpoint(
    "https://www.furaffinity.net/journals/{username}",
    rate_limit_ms=default_rate_limit
)

frontPageEndpoint = Endpoint(
    "https://www.furaffinity.net/",
    rate_limit_ms=default_rate_limit
)

