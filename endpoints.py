import requests
import time

class Endpoint():

    def __init__(self, baseURL, rate_limit_ms = 0):
        self.baseURL = baseURL
        self.rate_limit_ms = rate_limit_ms
        self.lastrun = 0

    def resolve_url(self, args):
        """Resolves the baseurl placeholders using the values in the dictionary `args`"""
        if (args is not None):
            resolvedUrl = self.baseURL.format(**args)
            return resolvedUrl
        return self.baseURL
    
    def send(self, args=None, wait = True) -> requests.Response:
        """
        Sends the request with the given arguments in `args`, subject to the rate limit.
        If the time since last run is less than the rate limit, then by default it will wait
        until the rate limit period has elapsed, then send the request (is `wait` is True).
        If `wait` is False, then this will throw a RateLimitException instead.
        """

        ## First, check the rate limit
        self.checkRateLimit(wait)

        ## Actually send the request
        resolved_url = self.resolve_url(args)
        response = requests.get(resolved_url)

        return response

        
    def checkRateLimit(self, wait):
        invocation_time = time.time_ns() // 1_000_000
        delta = invocation_time - self.lastrun
        self.lastrun = invocation_time
        if (delta < self.rate_limit_ms):
            ## Not enough time has passed to satisfy the timeout
            if wait:
                ## Wait until the rate limit period has passed
                wait_ms = self.rate_limit_ms - delta
                time.sleep(wait_ms / 1000)
            else:
                ## Or throw a RateLimitException instead
                raise RateLimitException()



class RateLimitException(Exception):
    pass

if __name__ == "__main__":
    galleryEndpoint = Endpoint(
        "https://www.furaffinity.net/user/{username}",
        rate_limit_ms=1000
    )
    journalEndpoint = Endpoint(
        "https://www.furaffinity.net/journals/{username}",
        rate_limit_ms=1000
    )

    cougrJournals = journalEndpoint.send(args = {
        "username": "cougr"
    }) 

    print(cougrJournals.text)
