from PIL import Image
from getpass import getpass
import datetime
import pickle

## Creates a visualization of a url2file dictionary created by faid.py
## In this visualization, each pixel represents a set of N submissions,
## where N is equal to `pixelDepth` below.  Submissions that are valid
## (that is, they still exist and are accessible) will add green color
## to the pixel, and submissions that are invalid or deleted will add
## red color to the pixel.  This may result in orange or yellow pixels
## if there is a mixture of valid and invalid submissions represented
## by a pixel.
##
## In the image, the first pixel (from left to right, top to bottom)
## represents submissions 0 to N-1, and the next pixel represents
## submissions N to 2N-1, and so on.
##
## Some pixels may represent submissions beyond the range of the latest
## submission.  Those pixels are colored grey. 

## TODO: move all to config file
mapfileName = "url2filemap.pkl"
latestSubmissionId = 44497164

resx = 10
resy = 10
resz = 26 - resx - resy  ## There are ~44 million submissions on FA; ceil(log2(44million)) = 26

## Ensure that dimensions[0] * dimensions[1] * pixelDepth < latestSubmission
dimensions = (2 ** resx, 2 ** resy)     ## should be < (1920,1080)
pixelDepth = 2 ** resz                  ## number of submissions to represent w/ each pixel

baseline = 100

numColorShades = resz
minColorAmount = 50     ## The minimum amount of color for a visible pixel, in any channel
						##   an empty channel will have 0, the first shade will be minColorAmount, 
						##   and the next shades N will be minColorAmount + (N-1)*colorIncrement
maxColorAmount = 255    ## The max amount of color for a visible pixel, in any channel
colorRange = maxColorAmount - minColorAmount
colorIncrement = int(maxColorAmount / numColorShades)

def main():
	url2file = None

	try:
		# load url2file map from pkl file, if exists
		mapfile = open(mapfileName, 'rb')
		url2file = pickle.load(mapfile)
		mapfile.close()
	except Exception as e:
		url2file = {}

	im = Image.new("RGB", dimensions)
	px = im.load()

	print ("working...")
	for y in range(dimensions[1]):
		## Each row (x)    is 1024 px wide, = 2^10, which can represent 10 bits
		## Each column (y) is 1024 px high, = 2^10, wihch can rep 10 bits
		## each pixel has a depth (z) of 2^(26-20) = 2^6 = 64

		if (y % 10 == 0):
			print(f"Calculating row {y}...")


		for x in range(dimensions[0]):
			linearPosition = dimensions[0]*y + x   ## e.g. (10,0) -> 10; (30,2) -> 3630; where dimensions[1] = 950

			## Average colors if this range contains valid submission numbers
			if linearPosition*pixelDepth < latestSubmissionId:  ## Does this pixel have any submissions in range
				submissions = range(linearPosition*pixelDepth, (linearPosition+1)*pixelDepth)  ## get all submissions for this pixel

				colors = []

				## For each submission in the range, add some color for the status of each submission 
				for url_id in submissions:
					if url_id > latestSubmissionId:
						pass
						## Do not add color so as to not disturb the average.
						## invalid submissions should not affect the pixel color
					elif url_id not in url2file:
						## Has not yet been checked - black
						colors.append((0,0,0))
					elif url2file[url_id] == None:
						## Checked and submsission deleted - red (will normalize 0-200)
						colors.append((colorIncrement,0,0))
					else:
						## Checked and submsission exists - green (will normalize 0-200)
						colors.append((0,colorIncrement,0))

				## mix colors array into single pixel value
				pixel = [
					# This commented code would average the values togeher, which might be better actually (if we use full color values to add)
					# sum(map(lambda r: r[0], colors))/len(colors),
					# sum(map(lambda g: g[1], colors))/len(colors),
					# sum(map(lambda b: b[2], colors))/len(colors)
					sum(map(lambda r: r[0], colors)),
					sum(map(lambda g: g[1], colors)),
					sum(map(lambda b: b[2], colors)),
					]

				## If a channel has a value, then fix it to make sure the first value added actually counts minColorAmount
				if pixel[0] > 0:
					pixel[0] = pixel[0] - colorIncrement + minColorAmount
				if pixel[1] > 0:
					pixel[1] = pixel[1] - colorIncrement + minColorAmount
				if pixel[2] > 0:
					pixel[2] = pixel[2] - colorIncrement + minColorAmount

				px[x,y] = tuple(pixel)			
			## If the entire range would consist of invalid submissions, output a grey color instead
			else:
				px[x,y] = (50,50,50)

	## Generate filename and save
	ts = datetime.datetime.now()
	timestampString = ts.strftime(f"%b-%d-%Y-%H-%M-%S")
	filename = f"faidv-{mapfileName}-{timestampString}.bmp"
	im.save(filename)


if __name__ == "__main__":
	main() 