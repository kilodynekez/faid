# Purpose
 
 `faid` attempts to correlate FurAffinity url ids with file ids.  Given a file id, we
 wish to be able to determine the submission id (and therefore the URL of the submission page)

           [Sumbission URL]                     [Downloaded file name]
    www.furaffinity.net/view/22609554       1487054757.kilodyne_bigpaw.png
                             ^^^^^^^^       ^^^^^^^^^^
       url id / submission id __|               |_____________ file id / timestamp

# Theory of Operation

 Some concepts:
  * URL ids are generated sequentially and are always increasing.
  * File Ids are generated based on the Unix timestamp of the submission.

 Therefore, if submission A was submitted before submission B, both the url id and 
 file ids will be smaller than those of submission B

 Every url id smaller than that of the most recently created submission is valid and 
 points to some submission, though there may be apparent gaps when submissions 
 have been deleted.

 Consider the following submission timeline, where progression along the timeline represents
 real time:

     time          8pm        9         10         11         12am        1          2          3
                   |          |          |          |          |          |          |          |
               |-------[A]--------[B]----------------------------[C]--------------[D]-------------|
                        |          |                              |                |                                     
     submission         |          file id: 1520994737            |                file id: 1521010217                            
                        |          url id:  26674366              |                url id:  26674368                             
                        |                                         |                                                
                        file id: 1520990815                       file id: 1521004517                            
                        url id:  26674365                         url id:  26674367                              
     
 There may be gaps in the timestamps, but file ids always are generated sequentially

 The main idea of this program is to query FA submission pages to get a sample set of file/url id
 relationships.  For example, if we were to query the URL ids 26674365 (A) and 26674368 (D), we would
 find the file ids 1520990815 (A) and 1521010217 (D).  We would therefore know that the file with id
 1521004517 (C) would have to have a url id between 26674365 (A) and 26674368 (D).  In this way, if we
 can find a large sample set of file/url id pairs, we can narrow down the possible url ids for a Given
 file id to a small range, which we can then search using a binary search.


 ## Array Metaphor

 These url ids may be seen as indices of an array, the values of which are the 
 corresponding file ids:

       url id      file id
    A[1]        = 1133739096
    ...
    A[26674365] = 1520990815
    A[26674366] = 1520994737
    A[26674367] = 1521004517
    A[26674368] = 1521010217

 Because the file ids are monotonically increasing with
 the url ids, the array is 'sorted'.  Thus, to find the index (url id) of a 
 given value (file id) in the array, we can use a binary search.  

 To find the value of a file id for a given url id (that is, to find A[url_id]),
 we must make a HTTP request to the FA submission page using that url id and
 scrape the resulting HTML to find the file id.  This is accomplished by the
 function getFileFromUrl(url_id) 

 # Outstanding Concerns

  - Sometimes we may find that multiple files have the same timestamp, because they were uploaded during the same second.  Currently we assume that any file with a matching timestamp is the correct result.  Instead, we should take into account the uploader account name, which is also present in the filename, and only select the result that was uploaded from that same account.
    - Ex: the file `1636740096.kilodyne_lisastompsnua1.png` currently produces a result of `44590782`, though the correct result should be `44590783`
 

 # Setup

 This expects Python 3 or higher, and has been developed with Python 3.9.

 ## Dependencies
 To install dependencies, simply run: 

 `python -m pip install requirements.txt`

 ## Cookie File
 
 Replace the contents of the `cookie` file with the value of your 'cookie' header on a request to FA after logging in.  This is a lazy way of authenticating with FA in lieu of a proper API or login auth procedure.
   - Log in to FA
   - Open up your browser console (F12 in Chrome)
   - Look in the Network tab
   - Refresh the page and click on the top result
   - In the Request Headers section, look for a line that says 'cookie: [lots of garbled text]'
   - Copy everything after 'cookie:' and paste it into the `cookie` file, replacing the original contents.
   - (DO NOT COMMIT YOUR COOKIE FILE!)

